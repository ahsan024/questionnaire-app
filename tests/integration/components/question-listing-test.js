import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('question-listing', 'Integration | Component | question listing', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{question-listing}}`);

  assert.equal(this.$('.question-desc').text().trim(), 'Description:', 'starts empty');
});

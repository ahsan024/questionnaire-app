import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('questionnaire-listing', 'Integration | Component | questionnaire listing', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{questionnaire-listing}}`);

  assert.equal(this.$('.questionnaire-title').text().trim(), 'Title:', 'starts empty');

  // assert.equal(this.$('.questionnaire-title').text().trim(), 'Title:', 'starts empty'));
  //
  // // Template block usage:
  // this.render(hbs`
  //   {{#questionnaire-listing}}
  //     template block text
  //   {{/questionnaire-listing}}
  // `);
  //
  // assert.equal(this.$().text().trim(), 'template block text');
});

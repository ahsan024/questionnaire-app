import { test } from 'qunit';
import moduleForAcceptance from 'ember-test/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | questionnaire');

test('visiting /', function(assert) {
  visit('/');

  andThen(function() {
    assert.equal(currentURL(), '/');
  });
});

test('should list available questionnaires.', function (assert) {
  visit('/');
  andThen(function () {
    assert.ok(find('.questionnaire-listing').length >= 1, 'should see atleast one listing');
  });
});

test('should redirect to the questionnaire page.', function (assert) {
  visit('/');
  click('a:contains("Start")');
  andThen(function () {
    assert.equal(currentRouteName(), 'questionnaire');
    // assert.equal(currentURL(), '/questionnaire/...', 'should navigate to questionnaire page');
  });
});

test('should see alert.', function (assert) {
  visit('/questionnaire/questionnaire-test');
  andThen(function () {
    click('.btn-success');
    andThen(function () {
      assert.ok(find('.alert'), 'should see the alert');
    });
  });
});

test('should successfully fill in the questionnaire.', function (assert) {
  visit('/questionnaire/questionnaire-test');
  andThen(function () {
    assert.equal(currentRouteName(), 'questionnaire');
    assert.ok(find('label:contains("Less than 6 months")'), 'should see the radio');
    click('label:contains("Less than 6 months")');
    click('button:contains("Submit")');
    andThen(function () {
      assert.ok(find('.alert'), 'should see the alert');
    });
    assert.ok(find('label:contains("Better price")'), 'should see the checkbox');
    click('label:contains("Better price")');
    click('button:contains("Submit")');
    andThen(function () {
      assert.ok(find('.alert'), 'should see the alert');
    });
    assert.ok(find('.ember-text-area'), 'should see the textarea');
    fillIn('.ember-text-area', "HELLO WORLD");
    click('button:contains("Submit")');
    andThen(function () {
      assert.ok(find('.alert'), 'should see the alert');
      assert.equal(currentRouteName(), 'success');
    });
  });
});

import { moduleForModel, test } from 'ember-qunit';
import Ember from 'ember';

moduleForModel('choice', 'Unit | Model | choice', {
  // Specify the other units that are required for this test.
  needs: []
});


test('should own a question', function(assert) {
  const Choice = this.store().modelFor('choice');
  const relationship = Ember.get(Choice, 'relationshipsByName').get('question');

  assert.equal(relationship.key, 'question', 'has relationship with question');
  assert.equal(relationship.kind, 'belongsTo', 'kind of relationship is belongsTo');
});

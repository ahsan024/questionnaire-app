import { moduleForModel, test } from 'ember-qunit';
import Ember from 'ember';

moduleForModel('question', 'Unit | Model | question', {
  // Specify the other units that are required for this test.
  needs: []
});

test('should own a questionnaire', function(assert) {
  const Question = this.store().modelFor('question');
  const relationship = Ember.get(Question, 'relationshipsByName').get('questionnaire');

  assert.equal(relationship.key, 'questionnaire', 'has relationship with questionnaire');
  assert.equal(relationship.kind, 'belongsTo', 'kind of relationship is belongsTo');
});

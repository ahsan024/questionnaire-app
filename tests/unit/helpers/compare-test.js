
import { compare } from 'ember-test/helpers/compare';
import { module, test } from 'qunit';

module('Unit | Helper | compare');

test('Check for equality', function(assert) {
  let result = compare(['1','1']);
  assert.ok(result);
});


test('Check for inequality', function(assert) {
  let result = compare(['1','2']);
  assert.notOk(result);
});

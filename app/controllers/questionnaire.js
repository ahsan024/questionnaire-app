import Ember from 'ember';

Ember.questionnaireController = Ember.Controller.extend({
  currentQuestion: 1,

  init: function() {
    this._super();
    Ember.run.schedule("afterRender", this, function() {
      this.send("setup");
    });
  },

  actions: {
    setup() {
      this.currentQuestion = 1;
      var questions = this.get('model').get('questions');
      this.send('scrollTo', questions);
    },
    next: function(params) {
      this.currentQuestion++;
      if (this.currentQuestion > params.length) {
        this.currentQuestion = params.length;
        return;
      }
      this.send('scrollTo', params);
      Ember.$(".alert").addClass("hidden");
    },

    prev: function(params) {
      this.currentQuestion--;
      if (this.currentQuestion < 1) {
        this.currentQuestion = 1;
        return;
      }
      this.send('scrollTo', params);
      Ember.$(".alert").addClass("hidden");
    },
    submit: function(questions) {
      for (var i = 0; i < questions.length; i++) {
        var q = questions[i];
        if (q['mandatory'] === 1 && q['answer'] === '') {
          Ember.$("#" + q['num']).addClass("error-border");
          Ember.$(".alert").removeClass("hidden");
          return false;
        } else {
          Ember.$("#" + q['num']).removeClass('error-border');
        }
      }
      this.get('router').transitionTo('success');
    },
    scrollTo: function(questions) {
      var el = Ember.$('#q' + this.currentQuestion);
      var elOffset = el.offset().top;
      var elHeight = el.height();
      var offset = elOffset - (elHeight / 2);
      var speed = 700;

      Ember.$('html, body').animate({
        scrollTop: offset
      }, speed);
      for (var i = 1; i <= questions.length; i++) {
        var value = 0;
        if (i === this.currentQuestion) {
          value = 1;
        } else {
          value = 0.3;
        }
        Ember.$("#q" + i).fadeTo("slow", value, function() {});
      }
    }
  }
});


export default Ember.questionnaireController;

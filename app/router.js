import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('questionnaire', { path: '/questionnaire/:q_id' });
  this.route('success');
});

export default Router;

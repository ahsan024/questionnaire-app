import DS from 'ember-data';

export default DS.Model.extend({
  questionnaire: DS.belongsTo('questionnaire'),
  name: DS.attr(),
  num: DS.attr(),
  type: DS.attr(),
  title: DS.attr(),
  question: DS.attr(),
  mandatory: DS.attr(),
  desc: DS.attr(),
  choices: DS.attr()
});

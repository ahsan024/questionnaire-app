import DS from 'ember-data';

export default DS.Model.extend({
  question: DS.belongsTo('question'),
  num: DS.attr(),
  name: DS.attr(),
  desc: DS.attr(),
  value: DS.attr()
});

import Ember from 'ember';

export default Ember.Component.extend({

  actions: {
    radioButtonClicked(newValue) {
      this.question.answer = newValue;
    },

    checkBoxClicked(c_id) {
      if (!(this.question.answer instanceof Array) || this.question.answer.length === 0) {
        this.question.answer = [];
        this.question.answer[0] = c_id;
      } else if (this.question.answer instanceof Array) {
        var index = this.question.answer.indexOf(c_id);
        if (index >= 0) {
          this.question.answer.splice(index, 1);
        } else {
          this.question.answer.push(c_id);
        }
      }
    },

    checkText(text) {
      if(text !== this.question.answer){console.log("DUBUG: error");}
    }
  }
});

import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    startQuestionnaire(questionnaire) {
      this.get('router').transitionTo('questionnaire', questionnaire.id);
    }
  }
});

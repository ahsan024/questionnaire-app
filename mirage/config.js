import Ember from 'ember';
export default function() {
  this.namespace = '/api';
  this.timing = 400; // simulate network delay


  this.get('/questionnaires', function() {
    if (Ember.testing) {
      return {
        data: testQuestionnaires
      };
    }
    return {
      data: questionnaires
    };
  });

  this.get('/questionnaires/:id', function(db, request) {
    if (Ember.testing) {
      return {
        data: testQuestionnaires.find((questionnaire) => 'questionnaire-test' === questionnaire.id)
      };
    }
    return {
      data: questionnaires.find((questionnaire) => request.params.id === questionnaire.id)
    };
  });

  let testQuestionnaires = [{
    type: 'questionnaires',
    id: 'questionnaire-test',
    attributes: {
      name: 'questionnaire-1',
      type: 'questionnaire',
      title: 'Customer Satisfaction Questionnaire',
      desc: 'Description of the questionnaire',
      questions: [{
          num: 'q1',
          name: 'question-1',
          type: '2',
          title: 'Question 1',
          mandatory: 1,
          question: 'How long have you used our products/service?',
          desc: 'Question description',
          answer: '',
          choices: [{
              num: 1,
              name: '',
              desc: 'Less than 6 months',
              value: 0
            },
            {
              num: 2,
              name: '',
              desc: '1 year to less than 3 years',
              value: 0
            },
            {
              num: 3,
              name: '',
              desc: '3 years to less than 5 years',
              value: 0
            },
            {
              num: 4,
              name: '',
              desc: '5 years or more',
              value: 0
            }
          ]
        },
        {
          name: 'question-2',
          num: 'q2',
          type: '3',
          title: 'Question 2',
          mandatory: 1,
          question: 'If you switched to another service provider, please indicate your reasons for doing so. Select all that apply.',
          desc: 'Question description',
          answer: '',
          choices: [{
              num: 1,
              name: '',
              desc: 'Better quality of service',
              value: 0
            },
            {
              num: 2,
              name: '',
              desc: 'Service is easier to use',
              value: 0
            },
            {
              num: 3,
              name: '',
              desc: 'Better price',
              value: 0
            },
            {
              num: 4,
              name: '',
              desc: 'Better payment plan',
              value: 0
            }
          ]
        },
        {
          name: 'question-3',
          num: 'q3',
          type: '1',
          title: 'Question 3',
          mandatory: 1,
          question: 'Do you have any suggestions for improving our products/services?',
          desc: 'Open-ended text',
          answer: '',
          choices: []
        }
      ]
    }
  }];


  let questionnaires = [{
    type: 'questionnaires',
    id: 'questionnaire-a',
    attributes: {
      name: 'questionnaire-1',
      type: 'questionnaire',
      title: 'Customer Satisfaction Questionnaire',
      desc: 'Description of the questionnaire',
      questions: [{
          num: 'q1',
          name: 'question-1',
          type: '2',
          title: 'Question 1',
          mandatory: 1,
          question: 'How long have you used our products/service?',
          desc: 'Question description',
          answer: '',
          choices: [{
              num: 1,
              name: '',
              desc: 'Less than 6 months',
              value: 0
            },
            {
              num: 2,
              name: '',
              desc: '1 year to less than 3 years',
              value: 0
            },
            {
              num: 3,
              name: '',
              desc: '3 years to less than 5 years',
              value: 0
            },
            {
              num: 4,
              name: '',
              desc: '5 years or more',
              value: 0
            }
          ]
        },
        {
          name: 'question-2',
          num: 'q2',
          type: '2',
          title: 'Question 2',
          mandatory: 0,
          question: 'How frequently do you purchase from us?',
          desc: 'Question description',
          answer: '',
          choices: [{
              num: 1,
              name: '',
              desc: 'Every day',
              value: 0
            },
            {
              num: 2,
              name: '',
              desc: 'Every week',
              value: 0
            },
            {
              num: 3,
              name: '',
              desc: 'Every month',
              value: 0
            }
          ]
        },
        {
          name: 'question-3',
          num: 'q3',
          type: '3',
          title: 'Question 3',
          mandatory: 1,
          question: 'If you switched to another service provider, please indicate your reasons for doing so. Select all that apply.',
          desc: 'Question description',
          answer: '',
          choices: [{
              num: 1,
              name: '',
              desc: 'Better quality of service',
              value: 0
            },
            {
              num: 2,
              name: '',
              desc: 'Service is easier to use',
              value: 0
            },
            {
              num: 3,
              name: '',
              desc: 'Better price',
              value: 0
            },
            {
              num: 4,
              name: '',
              desc: 'Better payment plan',
              value: 0
            }
          ]
        },
        {
          name: 'question-4',
          num: 'q4',
          type: '1',
          title: 'Question 4',
          mandatory: 1,
          question: 'Do you have any suggestions for improving our products/services?',
          desc: 'Open-ended text',
          answer: '',
          choices: []
        }
      ]
    }
  }];

}
